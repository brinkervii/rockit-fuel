package net.brinkervii.rockit.fuel.request;

import lombok.Getter;
import net.brinkervii.rockit.fuel.util.FileUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipOperation implements Runnable {
	private final InputStream byteStream;
	private final File unzipDirectory;
	private final ArrayList<File> files = new ArrayList<>();
	@Getter
	private boolean success = false;
	private final boolean keepBytes;

	private final ByteArrayOutputStream byteArrayOutputStream;

	public UnzipOperation(InputStream byteStream, boolean keepBytes) throws IOException {
		this.byteStream = byteStream;
		this.unzipDirectory = FileUtil.getTemp(String.format("UnzipOperation-%d", new Date().getTime()));
		this.keepBytes = keepBytes;

		this.byteArrayOutputStream = keepBytes ? new ByteArrayOutputStream() : null;
	}

	public UnzipOperation(InputStream byteStream) throws IOException {
		this(byteStream, false);
	}

	private InputStream stream() {
		if (keepBytes) {
			return new InputStream() {
				@Override
				public int read() throws IOException {
					final int read = byteStream.read();
					byteArrayOutputStream.write(read);
					return read;
				}
			};
		} else {
			return byteStream;
		}
	}

	@Override
	public void run() {
		try {
			try (ZipInputStream zipInputStream = new ZipInputStream(stream())) {
				ZipEntry zipEntry = zipInputStream.getNextEntry();
				while (zipEntry != null) {
					final File outputFile = new File(unzipDirectory, zipEntry.getName());
					try (FileOutputStream fos = new FileOutputStream(outputFile)) {
						byte[] buffer = new byte[1024];
						int len;

						while ((len = zipInputStream.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}

						files.add(outputFile);
					}


					zipEntry = zipInputStream.getNextEntry();
				}
			}

			this.success = true;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public byte[] getStoredBytes() {
		if (keepBytes) {
			return byteArrayOutputStream.toByteArray();
		}

		return null;
	}

	public Stream<File> files() {
		return files.stream();
	}
}
