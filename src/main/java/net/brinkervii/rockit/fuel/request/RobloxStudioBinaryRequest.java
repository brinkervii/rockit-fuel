package net.brinkervii.rockit.fuel.request;

import lombok.Getter;
import okhttp3.OkHttpClient;

import java.io.File;

public class RobloxStudioBinaryRequest extends RobloxStudioArchiveRequest<RobloxStudioBinaryRequest> {
	private final static String FILE_NAME = "RobloxStudioBeta.exe";
	@Getter
	private File binaryFile;

	public RobloxStudioBinaryRequest(OkHttpClient client) {
		super(client);
		this.self = this;
	}

	public void download() throws InvalidRequestException {
		if (binaryFile != null) return;

		super.download();

		zip.files()
				.filter(file -> file.getName().equalsIgnoreCase(FILE_NAME))
				.forEach(file -> this.binaryFile = file);

		if (binaryFile == null)
			throw new InvalidRequestException("Did not get Roblox studio binary bytes");
	}
}
