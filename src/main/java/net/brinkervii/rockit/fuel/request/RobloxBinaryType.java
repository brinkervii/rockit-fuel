package net.brinkervii.rockit.fuel.request;

import lombok.Getter;

public enum RobloxBinaryType {
	WindowsStudio("WindowsStudio");

	@Getter
	private final String urlParam;

	RobloxBinaryType(String urlParam) {
		this.urlParam = urlParam;
	}
}
