package net.brinkervii.rockit.fuel.request;

import lombok.Getter;

public enum RobloxBranch {
	Roblox("roblox"),
	GameTest1("gametest1.robloxlabs"),
	GameTest2("gametest2.robloxlabs"),
	GameTest3("gametest3.robloxlabs"),
	GameTest4("gametest4.robloxlabs");

	@Getter
	private final String branchName;

	RobloxBranch(String name) {
		this.branchName = name;
	}

	public static RobloxBranch getDefault() {
		return Roblox;
	}
}
