package net.brinkervii.rockit.fuel.request;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.fuel.util.FuelCache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
public class RobloxStudioArchiveRequest<R extends RobloxStudioArchiveRequest> extends RobloxRequest<R> {
	private final static String CACHE_KEY_PARTICLE = "RobloxStudio.zip";
	private final static FuelCache CACHE = new FuelCache(RobloxStudioArchiveRequest.class.getSimpleName());

	protected static UnzipOperation zip = null;

	protected RobloxStudioArchiveRequest(OkHttpClient client) {
		super(client);
	}

	private String cacheKey() {
		return versionId + "-" + CACHE_KEY_PARTICLE;
	}

	protected void download() throws InvalidRequestException {
		if (zip != null) return;

		final String cacheKey = cacheKey();
		if (CACHE.contains(cacheKey)) {
			try {
				unzipBytes(new ByteArrayInputStream(CACHE.get(cacheKey)), false);
			} catch (IOException e) {
				throw new InvalidRequestException(e);
			}
		} else {
			performDownload();
		}

		if (zip == null) {
			throw new InvalidRequestException("Did not get zip file");
		}
	}

	private void performDownload() throws InvalidRequestException {
		final String url = setupUrl() + "-RobloxStudio.zip";

		final Request request = new Request.Builder()
				.url(url)
				.build();

		log.info("Starting Roblox studio zip download: " + url);
		try {
			final Response response = client.newCall(request).execute();
			final ResponseBody body = response.body();
			if (body == null) {
				return;
			}

			if (zip == null) {
				unzipBytes(body.byteStream(), true);
			} else {
				log.info("Roblox archive already present");
			}
		} catch (IOException e) {
			throw new InvalidRequestException(e);
		}
	}

	private void unzipBytes(InputStream byteStream, boolean keepBytes) throws InvalidRequestException, IOException {
		log.info("Unzipping roblox archive");
		final UnzipOperation unzipOperation = new UnzipOperation(byteStream, keepBytes);
		unzipOperation.run();

		final byte[] storedBytes = unzipOperation.getStoredBytes();
		if (storedBytes != null) {
			CACHE.store(cacheKey(), storedBytes);
		}

		if (unzipOperation.isSuccess()) {
			zip = unzipOperation;
		} else {
			throw new InvalidRequestException("Unzipping archive contents failed");
		}
	}

	public void clearCachedFile() {
		zip = null;
	}
}
