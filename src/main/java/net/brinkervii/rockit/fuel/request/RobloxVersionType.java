package net.brinkervii.rockit.fuel.request;

import lombok.Getter;

public enum RobloxVersionType {
	ClientVersionUpload("ClientVersionUpload"),
	VersionQtStudio("versionQTStudio");

	@Getter
	private final String urlParam;

	RobloxVersionType(String urlParam) {
		this.urlParam = urlParam;
	}
}
