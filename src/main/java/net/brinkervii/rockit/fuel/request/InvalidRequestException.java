package net.brinkervii.rockit.fuel.request;

import java.io.IOException;

public class InvalidRequestException extends Exception {
	public InvalidRequestException(IOException e) {
		super(e);
	}

	public InvalidRequestException(String message) {
		super(message);
	}
}
