package net.brinkervii.rockit.fuel.request;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.fuel.util.FuelCache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
public class ApiDumpRequest extends RobloxRequest<ApiDumpRequest> {
	private final static FuelCache CACHE = new FuelCache(ApiDumpRequest.class.getSimpleName());
	private final static String FILE_NAME = "API-Dump.json";

	public ApiDumpRequest(OkHttpClient client) {
		super(client);
		this.self = this;
	}

	private String cacheKey() {
		return versionId + "-" + FILE_NAME;
	}

	public String get() throws InvalidRequestException {
		final String cacheKey = cacheKey();

		if (CACHE.contains(cacheKey)) {
			try {
				return CACHE.getString(cacheKey);
			} catch (IOException e) {
				throw new InvalidRequestException(e);
			}
		} else {
			final String url = setupUrl() + "-" + FILE_NAME;

			final Request request = new Request.Builder()
					.url(url)
					.build();

			try {
				log.info("Downloading Roblox API: " + url);
				final Response response = client.newCall(request).execute();
				final String apiDumpString = responseToString(response);

				CACHE.store(cacheKey, apiDumpString.getBytes(StandardCharsets.UTF_8));
				return apiDumpString;
			} catch (IOException e) {
				throw new InvalidRequestException(e);
			}
		}
	}
}
