package net.brinkervii.rockit.fuel.generator;

public interface DefinitionGenerator {
	void generate();

	String getLuaString();
}
