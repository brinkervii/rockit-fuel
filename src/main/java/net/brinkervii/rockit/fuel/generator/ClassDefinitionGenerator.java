package net.brinkervii.rockit.fuel.generator;

import lombok.Getter;
import net.brinkervii.rockit.fuel.model.RobloxApiData;
import net.brinkervii.rockit.fuel.model.api.RobloxClass;
import net.brinkervii.rockit.fuel.model.api.RobloxEnum;
import net.brinkervii.rockit.fuel.model.api.RobloxFunctionParameter;
import net.brinkervii.rockit.fuel.model.api.RobloxType;
import net.brinkervii.rockit.fuel.model.reflection.ReflectionMetadataMember;

import java.util.ArrayList;

public class ClassDefinitionGenerator implements DefinitionGenerator {
	@Getter
	private String luaString = "";
	private final RobloxApiData data;

	public ClassDefinitionGenerator(RobloxApiData data) {
		this.data = data;
	}

	private String lineSeparator() {
		return System.lineSeparator();
	}

	@Override
	public void generate() {
		final StringBuilder stringBuilder = new StringBuilder();

		data.getRobloxAPI().getClasses().forEach(clazz -> {
			generateClassHeader(clazz, stringBuilder);
			generateProperties(clazz, stringBuilder);
			generateEvents(clazz, stringBuilder);
			generateCallbacks(clazz, stringBuilder);
			generateClassStub(clazz, stringBuilder);
			generateFunctions(clazz, stringBuilder);
		});

		this.luaString = stringBuilder.toString();
	}

	private void generateClassHeader(RobloxClass clazz, StringBuilder stringBuilder) {
		stringBuilder
				.append("---@class ")
				.append(clazz.getName());

		if (clazz.getSuperclass() != null) {
			stringBuilder
					.append(" : ")
					.append(clazz.getSuperclass());
		}
		stringBuilder.append(lineSeparator());
	}

	private void generateProperties(RobloxClass clazz, StringBuilder stringBuilder) {
		clazz.getMembers().stream()
				.filter(member -> member.getMemberType().equalsIgnoreCase("property"))
				.filter(member -> {
					if (member.hasTags()) return !member.getTags().contains("Deprecated");
					return true;
				})
				.forEach(member -> {
					stringBuilder
							.append("---@field ");

					if (member.getName().contains(" ")) {
						stringBuilder
								.append("[")
								.append(member.getName())
								.append("]");
					} else {
						stringBuilder.append(member.getName());
					}

					stringBuilder
							.append(" ")
							.append(prepareParameterType(member.getValueType()))
							.append(lineSeparator());
				});
	}

	private void generateEvents(RobloxClass clazz, StringBuilder stringBuilder) {
		clazz.getMembers().stream()
				.filter(member -> member.getMemberType().equalsIgnoreCase("event"))
				.filter(member -> {
					if (member.hasTags()) return !member.getTags().contains("Deprecated");
					return true;
				})
				.forEach(member -> {
					stringBuilder
							.append("---@field ")
							.append(member.getName())
							.append(" RBXScriptSignal")
							.append(lineSeparator());
				});
	}

	private void generateCallbacks(RobloxClass clazz, StringBuilder stringBuilder) {
		clazz.getMembers().stream()
				.filter(member -> member.getMemberType().equalsIgnoreCase("callback"))
				.filter(member -> {
					if (member.hasTags()) return !member.getTags().contains("Deprecated");
					return true;
				})
				.forEach(member -> {
					stringBuilder
							.append("---@field ")
							.append(member.getName())
							.append(" function")
							.append(lineSeparator());
				});
	}

	private void generateClassStub(RobloxClass clazz, StringBuilder stringBuilder) {
		stringBuilder
				.append(clazz.getName())
				.append(" = {}")
				.append(lineSeparator());
	}

	private void generateFunctions(RobloxClass clazz, StringBuilder stringBuilder) {
		clazz.getMembers().stream()
				.filter(member -> member.getMemberType().equalsIgnoreCase("function"))
				.forEach(member -> {
					final ArrayList<String> parameterList = new ArrayList<>();

					final ReflectionMetadataMember reflection = data.getReflectionMetadata().queryMember(clazz.getName(), member.getName());
					if (reflection != null) {
						stringBuilder
								.append("---")
								.append(reflection.summary())
								.append(lineSeparator());
					}

					stringBuilder
							.append("---@function ")
							.append(clazz.getName())
							.append(".")
							.append(member.getName())
							.append(lineSeparator());

					if (member.hasTags()) {
						if (member.getTags().contains("Deprecated")) {
							stringBuilder
									.append("---@deprecated")
									.append(lineSeparator());
						}
					}

					member.getParameters().forEach(parameter -> {
						final String name = this.prepareParameterName(parameter);
						parameterList.add(name);

						stringBuilder
								.append("---@param ")
								.append(name)
								.append(" ")
								.append(this.prepareParameterType(parameter.getType()))
								.append(lineSeparator());
					});

					final RobloxType returnType = member.getReturnType();
					if (returnType != null) {
						stringBuilder
								.append("---@return ")
								.append(prepareParameterType(returnType))
								.append(lineSeparator());
					}

					stringBuilder
							.append("function ")
							.append(clazz.getName())
							.append(":")
							.append(member.getName())
							.append("(");

					stringBuilder
							.append(String.join(", ", parameterList))
							.append(")")
							.append(lineSeparator())
							.append(lineSeparator())
							.append("end")
							.append(lineSeparator())
							.append(lineSeparator());
				});
	}

	private String prepareParameterType(RobloxType type) {
		switch (type.getName().toLowerCase()) {
			case "bool":
				return "boolean";
			case "float":
			case "int":
			case "int32":
			case "int64":
			case "double":
				return "number";
			case "dictionary":
			case "array":
				return "table";
			case "function":
				return "function";
		}

		for (RobloxEnum anEnum : data.getRobloxAPI().getEnums()) {
			if (anEnum.getName().equals(type.getName())) {
				return "Enum." + type.getName();
			}
		}

		return type.getName();
	}

	private String prepareParameterName(RobloxFunctionParameter parameter) {
		final String name = parameter.getName();
		if ("function".equals(name)) {
			return "_function";
		}

		return name;
	}
}
