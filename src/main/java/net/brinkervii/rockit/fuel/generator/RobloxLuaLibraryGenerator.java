package net.brinkervii.rockit.fuel.generator;

import lombok.Getter;
import net.brinkervii.rockit.fuel.model.RobloxApiData;
import net.brinkervii.rockit.fuel.model.autocomplete.*;

import java.util.concurrent.atomic.AtomicReference;

public class RobloxLuaLibraryGenerator implements DefinitionGenerator {
	private final RobloxApiData data;
	@Getter
	private String luaString;

	private String lineSeparator() {
		return System.lineSeparator();
	}

	public RobloxLuaLibraryGenerator(RobloxApiData data) {
		this.data = data;
	}

	@Override
	public void generate() {
		final StringBuilder stringBuilder = new StringBuilder();

		data.getAutocompleteDocument().libraries().forEach(library -> {
			if (library instanceof AutocompleteItemStruct) {
				appendItemStructLibrary((AutocompleteItemStruct) library, stringBuilder);
			}

			appendLibraryDefinition(library, stringBuilder);

			library.getMembers().forEach(member -> {
				if (member instanceof AutocompleteFunction) {
					appendFunction(library, (AutocompleteFunction) member, stringBuilder);
				} else if (member instanceof AutocompleteProperties) {
					appendProperties(library, (AutocompleteProperties) member, stringBuilder);
				}
			});
		});

		this.luaString = stringBuilder.toString();
	}

	private void appendProperties(AutocompleteLibrary library, AutocompleteProperties member, StringBuilder stringBuilder) {
		member.getProperties().forEach(property -> {
			if (property.getDescription() != null) {
				stringBuilder
						.append("--- ")
						.append(property.getDescription())
						.append(lineSeparator());
			}

			stringBuilder
					.append("---@type ")
					.append(prepareType(property.getType()))
					.append(lineSeparator());

			if (!(library instanceof AutocompleteCoreLibrary)) {
				stringBuilder
						.append(library.getName())
						.append('.');
			}

			stringBuilder
					.append(property.getName())
					.append(" = nil")
					.append(lineSeparator())
					.append(lineSeparator());
		});
	}

	private void appendItemStructLibrary(AutocompleteItemStruct library, StringBuilder stringBuilder) {
		if (!data.getRobloxAPI().containsClassName(library.getName())) {
			stringBuilder
					.append("---@class ")
					.append(library.getName())
					.append(lineSeparator());
		}
	}

	private void appendLibraryDefinition(AutocompleteLibrary library, StringBuilder stringBuilder) {
		if (library instanceof AutocompleteCoreLibrary) {
			return;
		}

		stringBuilder
				.append(library.getName())
				.append(" = {}")
				.append(lineSeparator())
				.append(lineSeparator());
	}

	private void appendFunction(AutocompleteLibrary library, AutocompleteFunction member, StringBuilder stringBuilder) {
		stringBuilder
				.append("--- ")
				.append(member.getDescription())
				.append(lineSeparator());

		final StringBuilder parameterString = new StringBuilder();
		member.getParameters().forEach(param -> {
			stringBuilder
					.append("---@param ")
					.append(param.getName())
					.append(' ')
					.append(prepareType(param.getType()));

			if (param.getDescription() != null) {
				stringBuilder
						.append(' ')
						.append(param.getDescription());
			}

			stringBuilder.append(lineSeparator());

			if (parameterString.length() > 0) {
				parameterString.append(", ");
			}

			parameterString.append(param.getName());
		});

		if (member.getReturns().size() > 0) {
			final AtomicReference<String> returnDescription = new AtomicReference<>(null);
			final StringBuilder returnParameterString = new StringBuilder();
			member.getReturns().forEach(ret -> {
				if (ret.getDescription() != null) {
					if (returnParameterString.length() > 0) {
						returnParameterString.append(", ");
					}

					returnParameterString.append(prepareType(ret.getType()));
					final String retDescription = ret.getDescription();
					if (!retDescription.isEmpty()) {
						returnDescription.updateAndGet(d -> d + " " + retDescription);
					}
				}
			});

			stringBuilder
					.append("---@return ")
					.append(returnParameterString.toString());

			if (returnDescription.get() != null) {
				if (returnDescription.get().length() > 0) {
					stringBuilder
							.append(' ')
							.append(returnDescription);
				}
			}

			stringBuilder.append(lineSeparator());
		}

		stringBuilder.append("function ");

		if (!(library instanceof AutocompleteCoreLibrary)) {
			stringBuilder
					.append(library.getName())
					.append(member.isStaticMember() ? "." : ":");
		}

		stringBuilder.append(member.getName())
				.append('(');


		stringBuilder
				.append(parameterString.toString())
				.append(')')
				.append(lineSeparator())
				.append("end")
				.append(lineSeparator())
				.append(lineSeparator());
	}

	private String prepareType(AutocompleteType type) {
		if (data.getRobloxAPI().containsEnumName(type.toString())) {
			return "Enum." + type.toString();
		}

		return type.toString();
	}
}
