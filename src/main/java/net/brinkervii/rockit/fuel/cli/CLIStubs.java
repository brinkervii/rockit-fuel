package net.brinkervii.rockit.fuel.cli;

import net.brinkervii.rockit.fuel.tasks.GenerateStubsTask;
import net.brinkervii.rockit.fuel.tasks.GenerateStubsTaskConfiguration;

import java.io.File;
import java.util.concurrent.Callable;

import static picocli.CommandLine.Command;
import static picocli.CommandLine.Option;

@Command(name = "stubs", description = "Generate autocompletion stubs")
public class CLIStubs implements Callable<Void> {
	@Option(names = {"-f", "--file"})
	private File file = new File(String.format("dist%sRoblox.zip", File.separator));


	@Override
	public Void call() throws Exception {
		final GenerateStubsTaskConfiguration configuration = new GenerateStubsTaskConfiguration();
		if (file != null)
			configuration.setOutputFile(file);

		final GenerateStubsTask task = new GenerateStubsTask(configuration);
		task.run();

		return null;
	}
}
