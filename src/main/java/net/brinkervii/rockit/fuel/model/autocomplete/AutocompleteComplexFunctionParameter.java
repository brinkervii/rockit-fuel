package net.brinkervii.rockit.fuel.model.autocomplete;

import org.jsoup.nodes.Element;

public class AutocompleteComplexFunctionParameter extends AutocompleteFunctionParameter {
	private AutocompleteType realType;

	public AutocompleteComplexFunctionParameter(Element e) {
		super(e);

		for (Element child : e.children()) {
			if (child.tagName().equalsIgnoreCase("type")) {
				this.realType = AutocompleteType.fromString(child.ownText().trim());
				break;
			}
		}
	}

	@Override
	public AutocompleteType getType() {
		if (realType != null) {
			return realType;
		}

		return super.getType();
	}
}
