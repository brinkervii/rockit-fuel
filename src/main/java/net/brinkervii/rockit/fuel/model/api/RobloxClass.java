package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.ArrayList;

@Data
@JsonSerialize
public class RobloxClass {
	private String MemoryCategory;
	private String Name;
	private String Superclass;
	private ArrayList<String> Tags;
	private ArrayList<RobloxClassMember> Members = new ArrayList<>();
}
