package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.Collection;

@Data
@JsonSerialize
public class RobloxAPI {
	private Collection<RobloxClass> Classes;
	private Collection<RobloxEnum> Enums;
	private double Version;

	public boolean containsClassName(final String name) {
		return Classes.stream()
				.map(RobloxClass::getName)
				.map(n -> n.equals(name))
				.reduce(false, (a, b) -> a || b);
	}

	public boolean containsEnumName(String name) {
		return Enums.stream()
				.map(RobloxEnum::getName)
				.map(n -> n.equals(name))
				.reduce(false, (a, b) -> a || b);
	}
}
