package net.brinkervii.rockit.fuel.model.api;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.ArrayList;

@Data
@JsonSerialize
public class RobloxEnum {
	private String Name;
	private ArrayList<RobloxEnumItem> Items;
}
