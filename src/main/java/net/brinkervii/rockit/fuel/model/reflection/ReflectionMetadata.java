package net.brinkervii.rockit.fuel.model.reflection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.util.ArrayList;
import java.util.Optional;

public class ReflectionMetadata {
	private final Document document;

	private ArrayList<ReflectionMetadataClass> classes = new ArrayList<>();
	private ArrayList<ReflectionMetadataEnumItem> enums = new ArrayList<>();

	public ReflectionMetadata(String source) {
		this.document = Jsoup.parse(source, "", Parser.xmlParser());
	}

	public ReflectionMetadata translate() {
		document.select(".ReflectionMetadataClass").forEach(classElement -> {
			final ReflectionMetadataClass clazz = new ReflectionMetadataClass();
			putProperties(classElement, clazz);

			classElement.select(".ReflectionMetadataMember").forEach(memberElement -> {
				final ReflectionMetadataMember member = new ReflectionMetadataMember(memberElement.parent().className());
				putProperties(memberElement, member);
				clazz.getMembers().add(member);
			});

			classes.add(clazz);
		});

		return this;
	}

	private void putProperties(Element classElement, ReflectionMetadataItem clazz) {
		classElement.select("Properties").forEach(propertiesElement -> propertiesElement.children().forEach(child -> {
			clazz.putProperty(child.attr("name"), child.ownText());
		}));
	}

	public ReflectionMetadataMember queryMember(String className, String memberName) {
		final Optional<ReflectionMetadataMember> find = classes.stream()
				.filter(clazz -> clazz.name().equalsIgnoreCase(className))
				.map(clazz -> clazz.getMembers().stream()
						.filter(member -> member.name().equalsIgnoreCase(memberName))
						.findFirst())
				.filter(Optional::isPresent)
				.map(Optional::get)
				.findFirst();

		return find.orElse(null);
	}
}
