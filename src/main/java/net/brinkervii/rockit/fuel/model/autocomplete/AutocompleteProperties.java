package net.brinkervii.rockit.fuel.model.autocomplete;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jsoup.nodes.Element;

import java.util.ArrayList;

@Data
@EqualsAndHashCode(callSuper = true)
public class AutocompleteProperties extends AutocompleteMember {
	private final ArrayList<AutocompleteProperty> properties = new ArrayList<>();

	public AutocompleteProperties(Element element) {
		super();

		processElement(element);
	}

	private void processElement(Element element) {
		element.children().forEach(propertyElement -> properties.add(new AutocompleteProperty(propertyElement)));
	}
}
