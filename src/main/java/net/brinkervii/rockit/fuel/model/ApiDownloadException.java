package net.brinkervii.rockit.fuel.model;

public class ApiDownloadException extends Throwable {
	public ApiDownloadException(Exception innerException) {
		super(innerException);
	}

	public ApiDownloadException(String s) {
		this(new Exception(s));
	}
}
