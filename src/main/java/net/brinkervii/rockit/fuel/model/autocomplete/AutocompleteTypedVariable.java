package net.brinkervii.rockit.fuel.model.autocomplete;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

@Data
@Slf4j
public class AutocompleteTypedVariable {
	protected final String name;
	protected final AutocompleteType type;
	protected final boolean staticMember;
	protected final String description;

	public AutocompleteTypedVariable(Element element) {
		if (element.hasAttr("name")) {
			final String nameAttr = element.attr("name");

			if (nameAttr.equals("function")) {
				this.name = "_function";
			} else {
				this.name = nameAttr;
			}
		} else {
			this.name = "";
		}

		if (element.hasAttr("static")) {
			this.staticMember = element.attr("static").equalsIgnoreCase("true");
		} else {
			this.staticMember = false;
		}

		final String textString = element.ownText().trim();
		if (textString.isEmpty()) {
			this.description = "";
		} else {
			this.description = textString;
		}

		final String tagName = element.tagName();
		this.type = AutocompleteType.fromString(tagName);
		if (this.type == null) {
			log.warn(String.format("No type found for parameter '%s'; type = %s", name, tagName));
		}
	}

	@Override
	public String toString() {
		return String.format("%s: %s", name, type.toString());
	}

	public String getName() {
		if (name.equals("...")) {
			return "__tuple__";
		}

		return name;
	}
}
