package net.brinkervii.rockit.fuel.model.autocomplete;

import org.jsoup.nodes.Element;

public class AutocompleteProperty extends AutocompleteTypedVariable {
	public AutocompleteProperty(Element element) {
		super(element);
	}
}
