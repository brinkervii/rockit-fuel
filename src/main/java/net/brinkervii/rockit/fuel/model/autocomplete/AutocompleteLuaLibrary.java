package net.brinkervii.rockit.fuel.model.autocomplete;

import org.jsoup.nodes.Element;

public class AutocompleteLuaLibrary extends AutocompleteLibrary {
	public AutocompleteLuaLibrary(Element element) {
		super();
		super.processElement(element);
	}

	@Override
	public String toString() {
		return name;
	}
}
