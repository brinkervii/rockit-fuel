package net.brinkervii.rockit.fuel.model.autocomplete;

import org.jsoup.nodes.Element;

public class AutocompleteFunctionParameter extends AutocompleteTypedVariable {
	public AutocompleteFunctionParameter(Element element) {
		super(element);
	}
}
