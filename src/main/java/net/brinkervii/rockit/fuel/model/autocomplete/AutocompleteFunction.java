package net.brinkervii.rockit.fuel.model.autocomplete;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.util.ArrayList;

@Data
@Slf4j
@EqualsAndHashCode(callSuper = true)
public class AutocompleteFunction extends AutocompleteMember {
	private final ArrayList<AutocompleteFunctionParameter> parameters = new ArrayList<>();
	private final ArrayList<AutocompleteReturnParameter> returns = new ArrayList<>();

	private String name = "Unnamed Function";
	private boolean staticMember = false;
	private String description = "";

	public AutocompleteFunction(Element element) {
		super();

		processElement(element);
	}

	private void processElement(Element element) {
		if (element.hasAttr("name")) {
			this.name = element.attr("name");
		} else {
			log.warn("Element has no name attribute associated with it");
		}

		if (element.hasAttr("static")) {
			this.staticMember = element.attr("static").equalsIgnoreCase("true");
		}

		element.children().forEach(this::onElementChild);
	}

	private void onElementChild(Element element) {
		final String tagName = element.tagName();

		switch (tagName) {
			case "parameters":
				element.children().forEach(e -> {
					if (e.tagName().equalsIgnoreCase("parameter")) {
						parameters.add(new AutocompleteComplexFunctionParameter(e));
					} else {
						parameters.add(new AutocompleteFunctionParameter(e));
					}
				});
				break;

			case "returns":
				element.children().forEach(e -> returns.add(new AutocompleteReturnParameter(e)));
				break;

			case "description":
				this.description = element.ownText();
				break;
			default:
				log.warn("Unknown child element tag name " + tagName);
		}
	}

	@Override
	public String toString() {
		return String.format("function %s(%s)", name, parameters.toString());
	}
}

