package net.brinkervii.rockit.fuel.model.autocomplete;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.nodes.Element;

import java.util.ArrayList;

@Slf4j
@Data
public abstract class AutocompleteLibrary {
	protected String name = "Unnamed Library";
	protected final ArrayList<AutocompleteMember> members = new ArrayList<>();

	protected void processElement(Element element) {
		if (element.hasAttr("name")) {
			this.name = element.attr("name");
		}

		element.children().forEach(el -> {
			final String tagName = el.tagName();

			switch (tagName) {
				case "Function":
					members.add(new AutocompleteFunction(el));
					break;
				case "Properties":
					members.add(new AutocompleteProperties(el));
					break;
				default:
					log.warn("Unknown tag name " + tagName);
					break;
			}
		});
	}
}
