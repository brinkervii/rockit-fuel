package net.brinkervii.rockit.fuel.model.reflection;

import lombok.Getter;

public class ReflectionMetadataMember extends ReflectionMetadataItem {
	@Getter
	private final String memberClass;

	public ReflectionMetadataMember(String memberClass) {
		this.memberClass = memberClass;
	}

	public String name() {
		return getProperty("Name");
	}

	public String summary() {
		return getProperty("summary");
	}

	public boolean deprecated() {
		return getBooleanProperty("Deprecated");
	}

	public String constraint() {
		return getProperty("Constraint");
	}

	public String scriptContext() {
		return getProperty("ScriptContext");
	}
}
