package net.brinkervii.rockit.fuel.rbxautocomplete;

import org.luaj.vm2.*;
import org.luaj.vm2.lib.jse.LuajavaLib;

import java.io.InputStream;
import java.io.InputStreamReader;

public class LuaRequireShim extends LuaFunction {
	private final Globals globals;
	private final String resPrefix;
	private LuaZLib zlib = null;

	public LuaRequireShim(Globals globals, String resPrefix) {
		this.globals = globals;
		this.resPrefix = resPrefix;
	}

	@Override
	public boolean isfunction() {
		return true;
	}

	@Override
	public LuaValue call(LuaValue luaValue) {
		if (luaValue instanceof LuaString) {
			return require(luaValue.toString());
		}

		return null;
	}

	@Override
	public Varargs invoke(Varargs args) {
		return LuajavaLib.varargsOf(new LuaValue[]{require(args.arg(1).toString())});
	}

	private LuaValue require(String s) {
		if (s.equalsIgnoreCase("zlib")) {
			if (zlib == null) {
				this.zlib = new LuaZLib();
			}

			return zlib;
		}

		String scriptPath = resPrefix + s;
		if (!scriptPath.endsWith(".lua")) {
			scriptPath += ".lua";
		}

		final InputStream script = getClass().getClassLoader().getResourceAsStream(scriptPath);
		if (script == null) {
			return null;
		}

		final LuaValue chunk = globals.load(new InputStreamReader(script), s);
		return chunk.call();
	}
}
