package net.brinkervii.rockit.fuel.rbxautocomplete;

import lombok.Getter;
import org.luaj.vm2.LuaFunction;
import org.luaj.vm2.LuaTable;
import org.luaj.vm2.LuaValue;

public class LuaIOShim extends LuaTable {
	@Getter
	private final QtExtract parent;

	public LuaIOShim(QtExtract parent) {
		this.parent = parent;

		set("write", new LuaFunction() {
			@Override
			public LuaValue call(LuaValue v) {
				ioWrite(v.toString());
				return v;
			}
		});
	}

	public void ioWrite(String s) {
		System.out.println(s);
	}
}
