package net.brinkervii.rockit.fuel.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@Slf4j
public class FuelCache {
	private final File cacheDirectory;

	public FuelCache(String id) {
		this.cacheDirectory = new File(baseDirectory(), id);
	}

	private File baseDirectory() {
		final File file = new File(getClass().getSimpleName());
		if (!file.exists()) {
			if (!file.mkdirs()) {
				log.warn("Failed to create cache directory " + file);
				return null;
			}
		}

		return file;
	}

	private File fileForItem(String name) {
		return new File(cacheDirectory, name);
	}

	public void store(String name, byte[] content) throws IOException {
		if (!cacheDirectory.exists()) {
			if (!cacheDirectory.mkdirs()) {
				throw new FileNotFoundException("Could not create directory " + cacheDirectory);
			}
		}

		log.info("Storing cache item " + name);
		Files.write(fileForItem(name).toPath(), content);
	}

	public boolean contains(String name) {
		return fileForItem(name).exists();
	}

	public byte[] get(String name) throws IOException {
		log.info("Using cache item " + name);
		return Files.readAllBytes(fileForItem(name).toPath());
	}

	public String getString(String name) throws IOException {
		return new String(get(name), StandardCharsets.UTF_8);
	}
}
