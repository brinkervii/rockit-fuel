package net.brinkervii.rockit.fuel.tasks;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.fuel.generator.ClassDefinitionGenerator;
import net.brinkervii.rockit.fuel.generator.DefinitionGenerator;
import net.brinkervii.rockit.fuel.generator.EnumDefinitionGenerator;
import net.brinkervii.rockit.fuel.generator.RobloxLuaLibraryGenerator;
import net.brinkervii.rockit.fuel.model.ApiDownloadException;
import net.brinkervii.rockit.fuel.model.RobloxApiData;
import okhttp3.OkHttpClient;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Slf4j
public class GenerateStubsTask extends FuelTask<GenerateStubsTaskConfiguration> {
	private final static String FILLER_PATH = "net/brinkervii/fuel-filler.lua";

	public GenerateStubsTask(GenerateStubsTaskConfiguration configuration) {
		super(configuration);
	}

	@Override
	public void run() {
		try {
			log.info("Starting stub generation task");
			final OkHttpClient httpClient = new OkHttpClient();
			log.info("Assembling API data");
			final RobloxApiData data = RobloxApiData.download(httpClient);

			log.info("Creating directory structure");
			final File outputFile = configuration.getOutputFile().getAbsoluteFile();
			final File parentFile = outputFile.getParentFile();
			if (!parentFile.exists()) {
				if (!parentFile.mkdirs()) {
					throw new FileNotFoundException(parentFile.toString());
				}
			}

			final ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outputFile));

			writeZipEntry(out, "filler.lua", Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(FILLER_PATH)));
			writeZipEntry(out, "classes.lua", () -> new ClassDefinitionGenerator(data));
			writeZipEntry(out, "enums.lua", () -> new EnumDefinitionGenerator(data));
			writeZipEntry(out, "lib_rbx.lua", () -> new RobloxLuaLibraryGenerator(data));

			out.close();

			log.info("Done");
		} catch (IOException e) {
			log.info("Something went wrong in the IO department");
			e.printStackTrace();
		} catch (ApiDownloadException e) {
			log.error("Failed to download api data");
			e.printStackTrace();
		}
	}

	private void writeZipEntry(ZipOutputStream out, String filename, Supplier<DefinitionGenerator> supplier) throws IOException {
		log.info("Generating " + filename);

		final DefinitionGenerator generator = supplier.get();
		generator.generate();
		final String luaString = generator.getLuaString();
		if (luaString.isEmpty()) {
			log.error("Generator for file returned an empty string " + filename);
			return;
		}

		final byte[] data = luaString.getBytes(StandardCharsets.UTF_8);
		writeZipEntry(out, filename, data);
	}

	private void writeZipEntry(ZipOutputStream out, String filename, InputStream inputStream) throws IOException {
		writeZipEntry(out, filename, inputStream.readAllBytes());
		inputStream.close();
	}

	private void writeZipEntry(ZipOutputStream out, String filename, byte[] data) throws IOException {
		final ZipEntry zipEntry = new ZipEntry(filename);
		out.putNextEntry(zipEntry);

		log.info("Writing " + filename);
		out.write(data, 0, data.length);
		out.closeEntry();
	}
}
