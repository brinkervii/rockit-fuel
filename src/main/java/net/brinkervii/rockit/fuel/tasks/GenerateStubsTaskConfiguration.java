package net.brinkervii.rockit.fuel.tasks;

import lombok.Data;

import java.io.File;

@Data
public class GenerateStubsTaskConfiguration extends FuelTaskConfiguration {
	private File outputFile;
}
