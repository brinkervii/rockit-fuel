package net.brinkervii.rockit.fuel.tasks;

public abstract class FuelTask<CONFIG extends FuelTaskConfiguration> implements Runnable {
	protected final CONFIG configuration;

	protected FuelTask(CONFIG configuration) {
		this.configuration = configuration;
	}
}
